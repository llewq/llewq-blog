<?php
	$date_format = get_option( 'date_format', get_site_option( 'date_format', 'Y-m-d' ) );
	$date_format = empty( $date_format ) ? 'Y-m-d' : $date_format;
?>

<?php if ( sbe_show_featured_image() && sbe_mail_template_has_post_thumbnail() ): ?>
	<?php sbe_mail_template_the_post_thumbnail( 'email' ); ?>
<?php endif; ?>

<div <?php echo sbe_show_featured_image() ? 'style="float:left;"' : ''; ?>>
	<h1 style="font-size: 36px; text-transform: uppercase; font-weight: 900; line-height: 36px;">
		<a style="color: #068587; text-decoration: none;" href="<?php sbe_mail_template_the_permalink(); ?>" target="_blank">
			<span style="font-family: 'Montserrat', sans-serif; font-style: normal; font-weight:900;"><?php the_title(); ?></span>
		</a>
	</h1>
	<div style="margin:1em 0;font-size: 13px;color:#000 !important;line-height: 23px;">
		<?php if ( sbe_mail_template_send_full_post() ): ?>
			<span style="font-family: 'Sans Source Pro', sans-serif; font-style: normal; font-weight:300;"><?php sbe_mail_template_the_content(); ?></span>
		<?php else: ?>
			<span style="font-family: 'Sans Source Pro', sans-serif; font-style: normal; font-weight:300;"><?php @the_excerpt(); ?></span>
		<?php endif; ?>
	</div>
</div>

<div style="clear:both;"></div>

<div style="margin:0em 0 2.2em 0;font-size: 13px;color:#9E9E9E !important; <?php echo sbe_show_featured_image() ? 'float:right;' : 'float:none;'; ?>">
	<span style="font-family: 'Playfair Display', serif; font-style: italic; font-weight: 400;">
		<?php printf( __( 'by %s on %s', INCSUB_SBE_LANG_DOMAIN ), get_the_author(), get_the_date( $date_format ) ); ?>
	</span>
</div>

<div style="clear:both;"></div>