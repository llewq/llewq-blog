<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="hero-image">
			<?php the_post_thumbnail(); ?>
			<?php get_template_part( 'partials/featured-image-social' ); ?>		
		</div>
	<?php } ?>

<div class="inner-wrapper">
	<article role="main">
		<div class="content-area">	
			<section class="author">
				<?php $author = get_the_author(); ?>
				<?php $datetime = get_the_date('Y-m-d'); ?>
				<?php $date = get_the_date('F j, Y'); ?>
				<span class="author-style">by</span> <?php echo $author; ?> <nobr><span class="author-style">on</span> <time datetime="<?php echo $datetime; ?>"><?php echo $date; ?></time></nobr>
				<hr/>
			</section>
			
			<hgroup>
				<h1><?php the_title(); ?></h1>
				<h6><?php echo(get_the_excerpt()); ?></h6>
			</hgroup>
			
			<?php the_content(); ?>
		</div>
		
		<!-- end of content-area -->
		
			<?php get_template_part( 'partials/share' ); ?>
			
			<section role="comments">
				<h5>Comments</h5>
				<hr/>
				<?php comments_template(); ?>
			</section>
	</article>
</div><!-- end of .inner-wrapper -->

<?php endwhile; else : ?>
<div class="article">
	<p><?php _e( 'Sorry, no posts matched your criteria. Would you like to try a search?' ); ?></p>
	<?php get_search_form( true ); ?>
</div>
<?php endif; ?>

<?php get_footer(); ?>
