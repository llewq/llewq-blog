<?php get_header(); ?>
<div class="hero-image">
	<img src="<?php echo get_template_directory_uri(); ?>/img/search.png"/>
</div>
		
<div class="inner-wrapper">
	
	<hgroup class="query">
		<h1>Search results for:</h1>
		<h6>"<?php echo get_search_query(); ?>"</h6>
	</hgroup>
	
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
	<article>
		<hgroup>
			<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
		</hgroup>
		
		<?php // print first 30 characters, even if excerpt is set
			$text = get_the_content('');
			$text = strip_shortcodes( $text );
			$excerpt_length = apply_filters( 'excerpt_length', 30 );
			$excerpt_more = apply_filters( 'excerpt_more', ' ' . '' );
			$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
		?>
		<p><?php echo $text; ?></p>
		<div class="link-wrapper">
			<a href="<?php the_permalink(); ?>">
				<div class="link-container">
					<div class="bottom-edge"></div><div class="side-edge"></div><div class="top-edge"></div>
					<h5>READ MORE</h5>
				</div>
			</a>
		</div>
	</article>

<?php endwhile; else : ?>
	<div class="article">
		<p><?php _e( 'Sorry, no posts matched your criteria. Would you like to try another search?' ); ?></p>
		<?php get_search_form( true ); ?>
	</div>
<?php endif; ?>

</div><!-- end .interior-wrapper -->

<?php get_footer(); ?>
