<?php get_header(); ?>


	
	<div class="featured-image fourohfour">
		<h1>404</h1>
	</div>
<article role="main">
	
	
	<hgroup>
		<h1>The Page You're Looking for is Missing</h1>
		<h6>Maybe I moved it, deleted it, or maybe the address you entered or link you clicked was bad.</h6>
	</hgroup>
	
	<p>A 404 is the error message you get when you try to access a page on a website that doesn't exist in that location anymore. Sometimes you can fix it by simply checking the address in the URL bar. If the name of the domain or page is incorrect, fixing it may get you to the page you're looking for. If that doesn't work, looking in the menu or using the site's search is a good way to find the info you're after.</p>
	
		
</article>


<?php get_footer(); ?>
