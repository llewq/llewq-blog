<?php get_header(); ?>
		
<div class="inner-wrapper">
	
	<hgroup class="homepage">
		<h1 class="logotype">LLEW<span class="logo-q">Q</span></h1>
		<h4><?php bloginfo('description'); ?></h4>
	</hgroup>
	
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
	<article class="homepage">
		<div class="featured-image">
			<?php the_post_thumbnail(); ?>
			<?php get_template_part( 'partials/featured-image-social' ); ?>		
		</div>
		<div class="content">
			<hgroup><a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a></hgroup>
			<p><?php the_excerpt(); ?></p>
			<div class="link-wrapper">
				<a href="<?php the_permalink(); ?>">
					<div class="link-container">
						<div class="bottom-edge"></div><div class="side-edge"></div><div class="top-edge"></div>
						<h5>READ MORE</h5>
					</div>
				</a>
			</div>
		</div>
	</article>

<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

</div><!-- end .interior-wrapper -->

<?php get_footer(); ?>
