<?php 
//------------------------------------------------------------------------------
//
//		enqueque styles
//
//------------------------------------------------------------------------------

function llewq_enqueue_style() {
	wp_enqueue_style( 'main', '/wp-content/themes/llewq/css/theme_styles.css', false, false ); 
}

add_action( 'wp_enqueue_scripts', 'llewq_enqueue_style' );

wp_register_style(
    'syntaxhighlighter-theme-llewq',
    content_url( 'themes/llewq/css/syntax-highlighter_styles.css' ),
    array( 'syntaxhighlighter-core' ),
    '1.0.0'
);

add_filter( 'syntaxhighlighter_themes', 'syntax_theme_llewq' );
 
function syntax_theme_llewq( $themes ) {
    $themes['llewq'] = 'LLEWQ';
 
    return $themes;
}

//------------------------------------------------------------------------------
//
//		add support for post thumbnails
//
//------------------------------------------------------------------------------

add_theme_support( 'post-thumbnails' );

function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

add_image_size( 'email', 150, 112, true );

//------------------------------------------------------------------------------
//
//		add <p> tags to content entered in editor
//
//------------------------------------------------------------------------------

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');

//------------------------------------------------------------------------------
//
//		add HTML5 theme support
//
//------------------------------------------------------------------------------

function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
?>