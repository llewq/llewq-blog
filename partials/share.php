<section class="social-share">
	<h5>Share</h5>
	<hr/>
	<ul>
		<a onClick="window.open('https://twitter.com/intent/tweet?text=<?php the_title(); ?>&via=llewq&url=<?php the_permalink(); ?>','','resizable,height=254,width=550'); return false;">
			<li class="twitter-icon">
				<svg width="26px" height="21px" viewBox="0 0 26 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
				    <title>twitter-icon</title>
				    <defs></defs>
				    <g id="Article" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
				        <g id="Desktop" sketch:type="MSArtboardGroup" transform="translate(-511.000000, -390.000000)" fill="#FFFFFF">
				            <g id="image" sketch:type="MSLayerGroup" transform="translate(0.000000, 50.000000)">
				                <g id="social" transform="translate(506.000000, 332.000000)" sketch:type="MSShapeGroup">
				                    <path d="M27.716087,13.2346957 C27.7317391,13.4546087 27.7317391,13.6745217 27.7317391,13.8952174 C27.7317391,20.6107826 22.6205217,28.3484348 13.2785217,28.3484348 C10.4008696,28.3484348 7.72669565,27.5149565 5.47826087,26.067913 C5.88678261,26.1148696 6.28043478,26.1313043 6.7046087,26.1313043 C9.07982609,26.1313043 11.2656522,25.3291304 13.0116522,23.9603478 C10.778087,23.9133913 8.90686957,22.4506957 8.262,20.4378261 C8.5766087,20.4847826 8.89121739,20.516087 9.22147826,20.516087 C9.67773913,20.516087 10.1332174,20.4534783 10.5581739,20.3431304 C8.22991304,19.8712174 6.48469565,17.8270435 6.48469565,15.357913 L6.48469565,15.2945217 C7.16086957,15.6725217 7.9473913,15.908087 8.78086957,15.9393913 C7.41286957,15.0276522 6.516,13.4702609 6.516,11.7093913 C6.516,10.7655652 6.768,9.90078261 7.20782609,9.14556522 C9.70904348,12.2282609 13.4671304,14.2411304 17.6822609,14.4610435 C17.604,14.0838261 17.5562609,13.6901739 17.5562609,13.2973043 C17.5562609,10.497913 19.8211304,8.2173913 22.6361739,8.2173913 C24.0988696,8.2173913 25.419913,8.83095652 26.348087,9.82173913 C27.4961739,9.60182609 28.5973043,9.17686957 29.5724348,8.5953913 C29.1952174,9.77478261 28.3922609,10.7655652 27.3396522,11.3947826 C28.3609565,11.2844348 29.3525217,11.0011304 30.2642609,10.6082609 C29.5724348,11.6146957 28.7076522,12.5115652 27.716087,13.2346957" id="twitter-icon"></path>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</li>
		</a>
		<a onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>','','resizable,height=254,width=550'); return false;">
			<li class="facebook-icon">
				<svg width="14px" height="27px" viewBox="0 0 14 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
					<title>facebook-icon</title>
				    <defs></defs>
				    <g id="Article" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
				        <g id="Desktop" sketch:type="MSArtboardGroup" transform="translate(-570.000000, -387.000000)" fill="#FFFFFF">
				            <g id="image" sketch:type="MSLayerGroup" transform="translate(0.000000, 50.000000)">
				                <g id="social" transform="translate(506.000000, 332.000000)" sketch:type="MSShapeGroup">
				                    <path d="M77.7615652,9.42730435 L75.2932174,9.42730435 C73.3586087,9.42730435 72.9962609,10.3554783 72.9962609,11.6921739 L72.9962609,14.6645217 L77.6050435,14.6645217 L76.9914783,19.3194783 L72.9962609,19.3194783 L72.9962609,31.2566087 L68.184,31.2566087 L68.184,19.3194783 L64.173913,19.3194783 L64.173913,14.6645217 L68.184,14.6645217 L68.184,11.235913 C68.184,7.25713043 70.6218261,5.08695652 74.1764348,5.08695652 C75.8746957,5.08695652 77.3373913,5.21217391 77.7615652,5.27556522 L77.7615652,9.42730435" id="facebook-icon"></path>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</li>
		</a>
		<a onClick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php echo(get_the_excerpt()); ?>&source=<?php echo site_url(); ?>','','resizable,height=254,width=550'); return false;">
			<li class="linkedin-icon">
				<svg width="24px" height="23px" viewBox="0 0 24 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
				    <title>linkedin-icon</title>
				    <defs></defs>
				    <g id="Article" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
				        <g id="Desktop" sketch:type="MSArtboardGroup" transform="translate(-614.000000, -388.000000)" fill="#FFFFFF">
				            <g id="image" sketch:type="MSLayerGroup" transform="translate(0.000000, 50.000000)">
				                <g id="social" transform="translate(506.000000, 332.000000)" sketch:type="MSShapeGroup">
				                    <g id="linkedin" transform="translate(108.000000, 6.000000)">
				                        <path d="M23.6700001,22.7399951 L23.6700001,13.9361975 C23.6700001,9.22457996 21.1622411,7.03479418 17.8173289,7.03479418 C15.0901054,7.03479418 13.8809158,8.5619193 13.2205298,9.62274885 L13.2652197,9.62274885 L13.2652197,7.38802237 L8.18394771,7.38802237 C8.18394771,7.38802237 8.24970167,8.82641107 8.18394771,22.7399951 L13.2652197,22.7399951 L13.2652197,14.1568935 C13.2652197,13.7152152 13.2862837,13.2509233 13.4192149,12.9185911 C13.7929592,12.0126209 14.6281198,11.0611376 16.0359945,11.0611376 C17.8623035,11.0611376 18.5887281,12.4542993 18.5887281,14.5344527 L18.5887281,22.7399951 L23.6700001,22.7399951 Z M5.38926236,7.38802237 L0.30799038,7.38802237 L0.30799038,22.7399951 L5.38926236,22.7399951 L5.38926236,7.38802237 Z M2.88150335,0 C1.14485888,0 0,1.15128345 0,2.65550885 C0,4.11479355 1.10016897,5.30958647 2.81574939,5.30958647 L2.83823668,5.30958647 C4.61957106,5.30958647 5.71945537,4.11479355 5.71945537,2.65550885 C5.69725274,1.15128345 4.61957106,0 2.88150335,0 L2.88150335,0 Z" id="linkedin-icon"></path>
				                    </g>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</li>
		</a>
		<a href="mailto:?&subject=<?php the_title(); ?>&body=Check%20out%20this%20article%20I%20found%20on%20http%3A//llewq.com%20-%20<?php the_permalink(); ?>">
			<li class="email-icon">
				<svg width="22px" height="17px" viewBox="0 0 22 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
				    <title>email-icon</title>
				    <defs></defs>
				    <g id="Article" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
				        <g id="Desktop" sketch:type="MSArtboardGroup" transform="translate(-665.000000, -391.000000)" fill="#FFFFFF">
				            <g id="image" sketch:type="MSLayerGroup" transform="translate(0.000000, 50.000000)">
				                <g id="social" transform="translate(506.000000, 332.000000)" sketch:type="MSShapeGroup">
				                    <path d="M178.255523,14.7636804 C176.465718,16.0029518 174.663349,17.2427944 172.884966,18.4934877 C172.138548,19.0103267 170.875861,20.0657062 169.947264,20.0657062 L169.923278,20.0657062 C168.994682,20.0657062 167.731995,19.0103267 166.985577,18.4934877 C165.207194,17.2427944 163.404825,16.0029518 161.625871,14.7636804 C160.811492,14.2125758 159.652174,12.9161951 159.652174,11.8716663 C159.652174,10.7466134 160.260959,9.7826087 161.488237,9.7826087 L178.382306,9.7826087 C179.380576,9.7826087 180.218369,10.60898 180.218369,11.6192432 C180.218369,12.9042021 179.265786,14.0635206 178.255523,14.7636804 L178.255523,14.7636804 Z M180.218369,24.1056169 C180.218369,25.115309 179.391998,25.9416804 178.382306,25.9416804 L161.488237,25.9416804 C160.478545,25.9416804 159.652174,25.115309 159.652174,24.1056169 L159.652174,14.9932597 C159.996543,15.3718942 160.386599,15.7048414 160.811492,15.99153 C162.716087,17.2884818 164.644096,18.5854336 166.515568,19.9509166 C167.479001,20.6624982 168.672585,21.534557 169.923278,21.534557 L169.947264,21.534557 C171.197958,21.534557 172.391542,20.6624982 173.354975,19.9509166 C175.226447,18.5968555 177.154456,17.2884818 179.070472,15.99153 C179.483944,15.7048414 179.874,15.3718942 180.218369,14.9932597 L180.218369,24.1056169 L180.218369,24.1056169 Z" id="email-icon"></path>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</li>
		</a>
	</ul>
</section>
